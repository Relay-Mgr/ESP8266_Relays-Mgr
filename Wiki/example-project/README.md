# Multimedia Shelfs backwards on TV
- 1* Shelf for a Box with 2* ESP8266, each NodeMCU with 1* PCF8574 and 1* 8-port Relay shield controlling 8* sockets
- 1* Shelf above for most of the devices (Apple TV, Android Movie Box, Raspberry HiFiBerry, USB Harddrive with MP3,...)

## 2 pathes with separated power sources, separated ESP8266 control
- the **1st path** - I call it **a-path** or **black-path** (black power cable) manages often used electrics like TV, Apple TV, Google Home, ...
- the **2nd path** - I call it **b-path** or **white-path** (white power cable) manages not so often used electrics like Chromecast Ultra, Android Movie Box, Raspberry HiFiBerry with USB-Drive (streaming Multi-Rooming MP3 Audio), DVD-Player, ...

![Shelf1](https://gitlab.com/Relay-Mgr/ESP8266_Relays-Mgr/blob/master/Images/install_01.jpg)
![Shelf2](https://gitlab.com/Relay-Mgr/ESP8266_Relays-Mgr/blob/master/Images/install_2.jpg)
![Shelf3](https://gitlab.com/Relay-Mgr/ESP8266_Relays-Mgr/blob/master/Images/install_3.jpg)

## used Hardware
- ESP8266 NodeMCU v3
- Kuman Relay Shield Module 8 Channels
- I2C Expander PCF8574
- distributer box
- power cables (black, white)
- terminal strips
- sockets
- wood shelfs
- angle-irons

### So in total:
- 2* ESP8266 NodeMCU v3
- 2* PCF8574
- 2* 8-port Relay shield
- 16* sockets

![NodeMCU](https://gitlab.com/Relay-Mgr/ESP8266_Relays-Mgr/blob/master/Images/hw_esp8266.png)
[Aliexpress Link](https://de.aliexpress.com/item/new-Wireless-module-CH340-NodeMcu-V3-Lua-WIFI-Internet-of-Things-development-board-based-ESP8266/32556303666.html?spm=a2g0s.9042311.0.0.laTK47)
![Kuman 8Relay Shield](https://gitlab.com/Relay-Mgr/ESP8266_Relays-Mgr/blob/master/Images/hw_kuman.png)
[Amazon Link](https://www.amazon.de/gp/product/B014L10Q52/ref=oh_aui_detailpage_o09_s00?ie=UTF8&psc=1)
![i2C PCF8574](https://gitlab.com/Relay-Mgr/ESP8266_Relays-Mgr/blob/master/Images/hw_i2c.png)
[Amazon Link](https://www.amazon.de/gp/product/B06W564ZSD/ref=oh_aui_detailpage_o00_s00?ie=UTF8&psc=1)
![distributor box](https://gitlab.com/Relay-Mgr/ESP8266_Relays-Mgr/blob/master/Images/hw_box.png)
[Amazon Link](https://www.amazon.de/gp/product/B001JMOOXW/ref=oh_aui_detailpage_o00_s00?ie=UTF8&psc=1)
![power cable black](https://gitlab.com/Relay-Mgr/ESP8266_Relays-Mgr/blob/master/Images/hw_pwrcableb.png)
[Amazon Link](https://www.amazon.de/Kopp-152410841-Schlauchleitung-VV-F-schwarz/dp/B00BBUYFR2/ref=sr_1_2?s=diy&ie=UTF8&qid=1521900557&sr=1-2&keywords=kopp+schlauchleitung+schwarz)
![power cable white](https://gitlab.com/Relay-Mgr/ESP8266_Relays-Mgr/blob/master/Images/hw_pwrcablew.png)
[Amazon Link](https://www.amazon.de/gp/product/B000VDHIVK/ref=oh_aui_detailpage_o06_s00?ie=UTF8&psc=1)

![terminal strip](https://gitlab.com/Relay-Mgr/ESP8266_Relays-Mgr/blob/master/Images/hw_wago.png)
[Amazon Link](https://www.amazon.de/Wago-Verbindungsklemme-221-413-50-St%C3%BCck/dp/B00JB3U9CG/ref=sr_1_4?s=diy&ie=UTF8&qid=1521900664&sr=1-4&keywords=wago+klemmen)

![cable fixes](https://gitlab.com/Relay-Mgr/ESP8266_Relays-Mgr/blob/master/Images/hw_cablefix.png)
[Amazon Link](https://www.amazon.de/gp/product/B006BJCQ7E/ref=oh_aui_detailpage_o05_s00?ie=UTF8&psc=1)
![sockets](https://gitlab.com/Relay-Mgr/ESP8266_Relays-Mgr/blob/master/Images/hw_socket.png)
[Amazon Link](https://www.amazon.de/gp/product/B00BGODH54/ref=oh_aui_detailpage_o09_s00?ie=UTF8&psc=1)

![Shelfs](https://gitlab.com/Relay-Mgr/ESP8266_Relays-Mgr/blob/master/Images/hw_shelf.png)
[Amazon Link](https://www.amazon.de/M%C3%B6belbauplatte-Regalbrett-Leimholz-Paulownia-1200/dp/B011A1KIBI/ref=sr_1_9?s=diy&ie=UTF8&qid=1521900748&sr=1-9&keywords=regalbretter+holz)

![angle-iron](https://gitlab.com/Relay-Mgr/ESP8266_Relays-Mgr/blob/master/Images/hw_angleiron.png)
[Amazon Link](https://www.amazon.de/GAH-Alberts-336219-Stuhlwinkel-galvanisch-verzinkt/dp/B001BMHSO2/ref=sr_1_10?s=diy&ie=UTF8&qid=1521900809&sr=1-10&keywords=winkeleisen+verzinkt)

 
