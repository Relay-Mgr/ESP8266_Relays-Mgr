# Welcome to the ESP8266-Relay wiki!

here you can find:
- an **example project** with a 8-port Relay shield to use with 8-sockets
- **how to's** explaining some details of:
  - web interfaces and how to use
  - technical details

use navigation on the right frame 


![example project](https://gitlab.com/Relay-Mgr/ESP8266_Relays-Mgr/tree/master/Wiki/example-project)
![example project](https://gitlab.com/Relay-Mgr/ESP8266_Relays-Mgr/tree/master/Wiki/Initial-Configuration)
